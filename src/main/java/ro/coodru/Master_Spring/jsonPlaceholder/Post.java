package ro.coodru.Master_Spring.jsonPlaceholder;

import lombok.Data;

@Data  // this is a lombok annotation, instead of @AllArgsConstructor, etc
public class Post {
    private final Integer userId;
    private final Integer id;
    private final String title;
    private final String body;

}
