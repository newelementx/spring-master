package ro.coodru.Master_Spring.infoapp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "info.app")
@Data  // this is a lombok annotation, instead of @AllArgsConstructor, etc
public class InfoApp {
    private String name;
    private String description;
    private String version;
}
