package ro.coodru.Master_Spring.customer;

import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// Merge si doar cu @Component
@Repository
@Primary
public interface CustomerRepository extends JpaRepository<Customer, Long> {


}
