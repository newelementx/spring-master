package ro.coodru.Master_Spring.customer;

import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

// Merge si doar cu @Component
@Repository(value = "fake")
public class CustomerFakeRepository implements CustomerRepo {

    @Override
    public List<Customer> getCustomers() {
        return Arrays.asList(new Customer(1L, "Tudorel Alex", "pass431", "email@gmail.com"),
                new Customer(2L, "Critina Ich", "123password", "email@gmail.com"));
    }
}
