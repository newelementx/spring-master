package ro.coodru.Master_Spring.customer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.coodru.Master_Spring.exception.NotFoundException;
import java.util.List;

// Merge si doar cu @Component
@Service
@Slf4j
public class CustomerService {
//    private final static Logger LOGGER = LoggerFactory.getLogger(CustomerService.class);

    public final CustomerRepository customerRepository;

//    @Autowired
//    public CustomerRepo customerRepo;

    public CustomerService(CustomerRepository customerRepository) {      // we can use @Qualifier("fake") CustomerRepo customerRepo
        this.customerRepository = customerRepository;
    }

    public List<Customer> getCustomers() {
        log.info("getCustomer was called");
        return customerRepository.findAll();
    }

    public Customer getCustomer(Long id) {
        return customerRepository.findById(id)
                .orElseThrow(() -> {
                    NotFoundException notFoundException = new NotFoundException("Customer with id= " + id + " not found");
                    log.error("error in getCustomer {}", id, notFoundException);
                    return notFoundException;
                });
    }
}
