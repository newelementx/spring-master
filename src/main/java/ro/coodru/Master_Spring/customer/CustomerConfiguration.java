package ro.coodru.Master_Spring.customer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import ro.coodru.Master_Spring.infoapp.InfoApp;

// Merge si doar cu @Component
@Configuration
public class CustomerConfiguration {

    @Value("${app.useFakeCustomerRepo:false}")      // so the default value will be false, if not provided
    private Boolean useFakeCustomerRepo;

    @Value("${info.company.name}")
    private String companyName;

    @Bean
    CommandLineRunner commandLineRunner(InfoApp infoApp) {
        return args -> {
            System.out.println("Command line runner works");
            System.out.println("To prove that injection from application.properties, works: " + companyName);
            System.out.println("To prove that @ConfigurationProperties actually works:\n" + infoApp);
        };
    }

    // used in CustomerService

//    @Bean
//    CustomerRepo customerRepo() {
//        System.out.println("useFakeCustomerRepo = " + useFakeCustomerRepo);
////        return useFakeCustomerRepo ? new CustomerFakeRepository() : new CustomerRepository();
//        return new CustomerFakeRepository();
//    }
}
