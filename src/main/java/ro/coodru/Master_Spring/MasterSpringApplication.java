package ro.coodru.Master_Spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class MasterSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasterSpringApplication.class, args);
	}

}
