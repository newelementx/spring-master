package ro.coodru.Master_Spring.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

@Data   // In place of @AllArgsConstructor, @Getter, @ToString - is a Lombok annotation
public class ApiException {

    private final String message;
    private final Throwable throwable;
    private final HttpStatus httpStatus;
    private final ZonedDateTime zonedDateTime;


}
