package ro.coodru.Master_Spring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

//@ResponseStatus(value = HttpStatus.NOT_FOUND)    // Or like this, if we choose not to use ExceptionHandler for this
public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }
}
